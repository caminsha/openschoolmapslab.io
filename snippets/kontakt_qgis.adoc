ifdef::hoeflichkeitsform[]
Noch Fragen? Wenden Sie sich an die QGIS-Community (https://www.qgis.org/de/site/forusers/support.html[])!
endif::[]

ifndef::hoeflichkeitsform[]
Noch Fragen? Wende dich an die QGIS-Community (https://www.qgis.org/de/site/forusers/support.html[])!
endif::[]
