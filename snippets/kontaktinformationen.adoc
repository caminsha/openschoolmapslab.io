ifdef::hoeflichkeitsform[]
Noch Fragen? Wenden Sie sich an OpenStreetMap Schweiz (info@osm.ch) oder Stefan Keller (stefan.keller@ost.ch)!
endif::[]

ifndef::hoeflichkeitsform[]
Noch Fragen? Wende dich an OpenStreetMap Schweiz (info@osm.ch) oder Stefan Keller (stefan.keller@ost.ch)!
endif::[]
