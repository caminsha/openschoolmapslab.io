= Mit uMap eine thematische Online-Karte erstellen
OpenSchoolMaps.ch -- Freie Lernmaterialien zu freien Geodaten und Karten
:hoeflichkeitsform:
:imagesdir: ../../bilder/
include::../../snippets/lang/de.adoc[]
include::../../snippets/suppress_title_page.adoc[]

*Eine Anleitung für Interessierte und Lehrpersonen*

:sectnums!:
== Übersicht

.Ziel
Das Ziel dieser Anleitung ist es, mit uMap eine thematische Online-Karte zu erstellen.

.Zielgruppe
Alle, die am Kartieren (Digitalisieren) und am Erstellen einer eigenen Karte interessiert sind.

.Zeitplanung
Je nach Methode und Anzahl Geodaten benötigt die Bearbeitung dieses Arbeitsblatts etwa 20-40 min.

.Kapitelübersicht
image::umap/online-karte_erstellen/uml_diagram.svg["SVG"]

:sectnums:
== Geodaten in uMap einbinden

Um zu beginnen, suchen Sie in OpenStreetMap nach den Geodaten, die Sie in uMap einbinden wollen.
Sie können Werkzeuge wie taginfo, das OpenStreetMap-Wiki oder tagfinder dazu verwenden.
Wenn sich die Geodaten, die Sie verwenden möchten, nicht in OpenStreetMap befinden,
können Sie mit dem übernächsten Kapitel <<Daten nicht in OpenStreetMap vorhanden>> fortfahren.

Andernfalls fahren Sie mit dem Kapitel <<Daten in OpenStreetMap vorhanden>> fort.

[NOTE]
====
Taginfo: https://taginfo.openstreetmap.ch/ +
OpenStreetMap-Wiki: https://wiki.openstreetmap.org/ +
Tagfinder: http://tagfinder.herokuapp.com/
====

== Daten nicht in OpenStreetMap vorhanden

Wenn sich die Daten, die Sie in uMap einbinden wollen, nicht in OpenStreetMap befinden,
haben Sie folgende Möglichkeiten:

* <<Daten manuell in uMap erfassen>>:
Wenn Sie nicht viele Daten zum Erfassen haben
* <<Geodaten erfassen>>:
Wenn Sie Ihre Daten in Textformat erfassen möchten
* <<Daten in OpenStreetMap erfassen>>:
Wenn die Daten für OpenStreetMap geeignet sind und Sie sie dort einpflegen wollen
* <<Daten als externe Datenquelle/Datei vorhanden>>:
Wenn Ihre Daten bereits in einem von uMap unterstützten Geo-Informations-Format vorliegen

=== Daten manuell in uMap erfassen

Sie können die Daten von Hand in uMap erfassen.
Hierbei spielt es eine Rolle, um wie viele Geodaten es sich handelt:
Je nach Menge der Geodaten kann der Webbrowser langsam werden oder sogar mangels Memory anhalten.
Schauen Sie sich dazu das Arbeitsblatt "Mit uMap einen Lageplan erstellen"
auf https://www.openschoolmaps.ch an, wenn Sie nur wenige Geodaten erfassen müssen.

=== Geodaten erfassen

Anstatt die Daten in uMap selbst zu erfassen, können Sie diese auch aus einer einfach zu erstellenden Datei einlesen.
Diese kann im textbasierten GeoJSON-Format vorliegen, das Sie z.B. auf der Website geojson.io oder mit QGIS Desktop erzeugen können.
Für letzteres sehen Sie das Arbeitsblatt "Verwaltung und Erfassung von Geodaten" in der "Einführung in QGIS 3" auf https://www.openschoolmaps.ch an.

geojson.io ist eine Webseite, die Ihnen ermöglicht, auf einer Karte Punkte,
Linien, Rechtecke und Polygone zu zeichnen und diese danach in Form eines GeoJSONs abzuspeichern.

Nachdem Sie Ihre Geodaten erfasst haben, kopieren Sie die Daten in die Zwischenablage
und fügen Sie sie in uMap unter _Import Data_ ein.

Wenn Sie eine Datei mit Geodaten importieren möchten, sehen Sie sich das Kapitel
<<Daten als externe Datenquelle/Datei vorhanden>> an.

[NOTE]
====
Stellen Sie sicher, dass Sie Ihre Daten in einer der folgenden Dateiformaten erfasst haben:
*geojson, csv, gpx, kml, osm, georss, umap*

Wir empfehlen Ihnen die Daten im GeoJSON-Format zu importieren.
Im GeoJSON-Format werden alle Attribute importiert.
Mit anderen Formaten werden möglicherweise nicht alle Attribute mit-importiert.

Wenn Sie sich entscheiden CSV zu brauchen, vergewissern Sie sich, dass Sie
eine Kolumne _lat_ und _lon_ haben.
Alle anderen Kolumnen werden als Attribute importiert.
====

.Beispiel wie man ein GeoJSON in uMap importiert
image::umap/online-karte_erstellen/umap_import_data.PNG["'Daten importieren'-Menü in uMap", 275, 306]

Unter _Wähle das Datenformat für den Import_ müssen Sie das Dateiformat Ihrer Datei angeben.

Mit der Schaltfläche _Importieren_ werden Ihre Dateien in uMap importiert.

=== Daten in OpenStreetMap erfassen

Eine weitere Möglichkeit ist, dass Sie die Daten gleich in OpenStreetMap erfassen.
Der Vorteil hier ist, dass die Daten damit in verschiedenen Diensten genutzt werden und allen zur Verfügung stehen.
Man beachte jedoch auch, dass die Daten nicht sofort zur Verfügung stehen, da sie zuerst
auf diese Dienste verteilt und verarbeitet werden müssen.

Das Arbeitsblatt "OpenStreetMap bearbeiten" auf https://www.openschoolmaps.ch erläutert, wie man die Daten in OpenStreetMap erfasst.
Wenn die Daten in OpenStreetMap erfolgreich erfasst worden sind, können Sie bei Kapitel <<Daten in OpenStreetMap vorhanden>> fortfahren.
Wie erwähnt, kann es einige Zeit dauern, bis die Daten verfügbar sind.

NOTE: Stellen Sie sicher, dass Ihre Daten auch für OpenStreetMap geeignet sind.
Das heisst *keine* persönliche Daten und *nur* objektive/überprüfbare Fakten.

== Daten als externe Datenquelle/Datei vorhanden

Wenn Sie die Geodaten als Datei vorhanden haben, können Sie die auch in uMap importieren.
Navigieren Sie dazu wieder in uMap unter _Daten importieren_ und klicken Sie auf
die _Durchsuchen..._ Schaltfläche.
Wählen Sie Ihre Datei aus und bestätigen Sie mit "Öffnen".

NOTE: Wiederum empfehlen wir Ihnen, wenn möglich, eine Datei im GeoJSON-Format zu importieren

Unter _Wähle das Datenformat für den Import_ müssen Sie das Dateiformat Ihrer Datei angeben.

Mit der Schaltfläche _Importieren_ werden Ihre Dateien in uMap importiert.

CAUTION: Vergewissern Sie sich, falls Ihre Daten vom Internet stammen, dass Sie gegen keine Lizenzen verstossen.

== Daten in OpenStreetMap vorhanden

=== Einen Snapshot der OSM-Daten

Um den aktuellen Stand der Daten in uMap zu importieren, verwenden Sie https://overpass-turbo.osm.ch/.
Mit dem Werkzeug Wizard können Sie nach bestimmten Tags in OpenStreetMap suchen.
Falls Sie nur bestimmte Attribute benötigen, können Sie das `[out:json]` zu einem `[out:csv()]` ändern.
So kommen die Daten zwar im CSV-Format zurück, jedoch können Sie so genau festlegen, welche Attribute zurückgegeben werden.
Das CSV-Format hat den Vorteil, dass es einfacher von Auge lesbar und von Hand editierbar ist.
Jedoch ist es zu empfehlen, JSON zu benutzen, falls Sie die Daten später noch in einem anderen Programm öffnen wollen.
Wenn Sie mit dem Ergebniss zufrieden sind, können Sie die Daten unter _Export_ als GeoJSON exportieren.
Falls Sie es im CSV-Format abgefragt haben, müssen Sie eine neue CSV-Datei erstellen und den Text dort hinein kopieren.

.Overpass-Turbo Abfrage für CSV-Daten aller Kinos der Schweiz
----
[out:csv(name, 'addr:street', 'addr:housenumber', website, ::"lat", ::"lon"; true; ",")];
nwr[amenity=cinema];
out center;
----

Wie Sie eine Datei in uMap importieren, sehen Sie im Kapitel <<Daten als externe Datenquelle/Datei vorhanden>>.


=== Daten immer auf dem aktuellsten Stand halten
Wenn Sie die Daten auf Ihrer uMap immer aktuell halten möchten, ist das ganze sehr ähnlich.
Öffnen Sie hierzu wieder https://overpass-turbo.osm.ch/ und suchen Sie mit dem Wizard nach z.B. *amenity=bar*.

Tauschen Sie nun den in Abbildung 3 rot markierten Teil mit *out center;* aus, da wir ja nur POIs angezeigt bekommen möchten.

.Query aus Overpass-Turbo bearbeiten
image::umap/online-karte_erstellen/overpass-turbo_query.PNG["Abfrage", 290, 395]

Nun gehen Sie unter _Export_ -> _Abfrage_ auf _Kopiere_ neben _als umap ausgelagerte Daten Url_.

.Query aus Overpass-Turbo exportieren
image::umap/online-karte_erstellen/overpass-turbo_export.PNG["Export Abfrage", 290, 395]

Ihnen wird der Query als URL in die Zwischenablage gespeichert, aber noch nicht die ganze für die Abfrage benötigte URL.
Klicken Sie einfach auf "OK, verstanden".
Gehen Sie nun in ihre uMap-Karte.
Editieren Sie die aktuelle Ebene (oder erzeugen Sie eine neue Ebene) indem Sie auf das Stift-Symbol klicken unter _Ebenen verwalten_.

.Ebenen editieren in uMap
image::umap/online-karte_erstellen/umap_manage_layers.PNG["'Ebenen verwalten'-Menü", 342, 288]

Im Editier-Menü öffnen Sie das Menü _Ausgelagerte Daten_ und fügen Sie die kopierte URL in das Textfeld URL.

Nachdem Sie das getan haben, fügen Sie das URL-Teil "*https://overpass-api.de/api/interpreter?data=*" noch vor dem Query Export ein.

Als _Format_ wählen Sie OSM aus und aktivieren Sie dazu noch die Schaltfläche _Dynamisch_.

.Ausgelagerte Daten in uMap eintragen
image::umap/online-karte_erstellen/umap_remote_data.PNG["'Ausgelagerte Daten'-Menü", 274, 532]

Unter _Von Zoomstufe_ können Sie noch einen Mindest-Zoom angeben, für den Fall, dass Sie viele Daten haben die uMap in angezeigt werden müssen.

CAUTION: Overpass-Turbo kann sehr schnell und einfach überlastet werden, was das Ausführen von Querys sehr langsam gestaltet oder sogar eine Fehlermeldung am oberen Ende des Bildschirms zur Folge hat, was zwar keinen Einfluss hat, aber nervig sein kann, dies kann aber mit einer erhöhten Zoomstufe behoben werden. Trotzdem sollte man immer die Anzahl dynamischer Layer und die Grösse der Querys im Auge behalten.

== uMap-Grafik konfigurieren

Sobald die Daten in der uMap importiert sind, gibt es noch viele Möglichkeiten, ihre Darstellung den eigenen Bedürfnissen anzupassen.
Zwei davon werden hier näher angeschaut.

=== Popup Template festlegen

Wenn die Icons der Daten angeklickt werden, wird Standartmässig Name und Beschreibung eines Objekt angezeigt.
Das heisst, in den Daten, die uMap übergeben wurden, wird geschaut ob es Attribute mit den Namen "name" und "description" gibt.
Sind diese Vorhanden, werden sie angezeigt, wenn nicht, wird stattdessen der Name des Layers angezeigt.
Um das anzupassen, öffnen Sie in den Einstellungen der Ebene die Interaktionsoptionen.
Unter der Option "Popup Vorlage" können Sie ein Template definieren, das festlegt wie das Vorschaufenster der Daten aussehen soll.
Um die Syntax zu sehen, klicken Sie auf das Fragezeichen-Icon.

.Standard Popup Template
image::umap/online-karte_erstellen/umap_popup_template.PNG["'Popup Template Option'-Menü"]

So können zum Beispiel Attribute in Links eingebaut werden, damit die Landeskarte oder ein Routenplaner bei den Koordinaten des Attributes geöffnet werden, und vieles mehr.


.Beispiel Template für eine Kinokarte
----
#{name}
Adresse: {addr:street} {addr:housenumber}
[[{website}|Website]]
Fahrplan: [[https://www.sbb.ch/de/kaufen/pages/fahrplan/fahrplan.xhtml?nach={name}|SBB]] | [[https://timetable.search.ch/..{name}.de.html|search.ch]]
Route: [[https://routing.osm.ch/?z=13&loc={lat},{lon}&loc={lat},{lon}|OSM.ch]] | [[https://map.search.ch/{lat},{lon}|Search.ch]]
Weitere Karten: [[http://map.geo.admin.ch/?swisssearch={lat},{lon}|GeoAdmin]] |  [[https://www.openstreetmap.org/?mlat={lat}&mlon={lon}#map=15/{lat}/{lon}|OSM.org]]
----

Mit doppelten eckigen Klammern werden Verlinkungen gemacht.
Mit den geschweiften Klammern werden Attribute vom jeweiligen Objekt eingefügt.
So wird in diesem Beispiel der Name und die Adresse angezeigt.
Hier werden auch noch die Längen- und Breitengrade des Objekts in Links zur Landeskarte und zu OpenStreetMap eingefügt,
um sie am richtigen Ort zu öffnen, und der Name wird benutzt, um den SBB-Fahrplan zu öffnen.
Alles was in der Verlinkung nach einem "|" geschrieben wird, ist der Text, der für den Link angezeigt wird.

=== Icons setzen
Um die uMap weiter zu individualiseren, kann das Icon angepasst werden.
Es setzt sich zusammen aus einer Hintergrundfarbe, einer Form, und einem Symbol in der Mitte.
Unter der Option Bildzeichenform kann zwischen einer Auswahl von Formen ausgewählt werden.
Unter Farbe kann die Hintergrundfarbe festgelegt werden.

.Auswahl der Bildzeichenform
image::umap/online-karte_erstellen/umap_icon_type_select.PNG[]

Das Symbol können Sie unter Bildschirmzeichensymbol ändern.
Dabei gibt es zwei Möglichkeiten ein Symbol zu wählen.

1. Sie wählen eines aus den Standarticons aus.

2. Mit einem Klick auf "Symbol festlegen" erscheint ein Texteingabefeld.
Darin können Sie einen Link zu einem Online verfügbaren Bild eintragen, wobei Sie darauf achten müssen,
dass das Bild verwendet werden darf.
Oder Sie fügen dort ein Unicodezeichen ein.
(https://unicode-table.com/de/)

.Standartsymbole
image::umap/online-karte_erstellen/umap_icon_symbol.PNG[]


== Abschluss

Mit einem Klick auf die blaue _Speichern_-Schaltfläche oben rechts und der _Bearbeiten deaktivieren_-Schaltfläche ist Ihre Karte nun fertig!

Wenn Sie fertig sind, könnte es etwa so aussehen:

.Beispielkarte von Kinos in der Schweiz (https://umap.osm.ch/de/map/kinos_3067)
image::umap/online-karte_erstellen/umap_resultat.PNG[]

Sie können nun Ihre Karte mit anderen teilen, in Webseiten einbinden und sie bleibt immer auf dem neusten Stand von OpenStreetMap!

---

include::../../snippets/kontakt_openstreetmap.adoc[]

include::../../snippets/license.adoc[]
